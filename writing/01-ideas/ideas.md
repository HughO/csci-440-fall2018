# Team Members

* Hugh O'Neill


# SQL Basics tutorial
The obejective of this tutorial is to give the reader a strong understanding of basic sql commands, and equip them with the ability to do the basics of database management with it. This will include 
learning commands, scripting, and understanding how learning SQL will help them in a practical way. 

This is important because SQL is a commonly used tool, which is also unintuitive enough to require learning how to use it. Thus, tutorials for it are naturally practical. 

# The basics of making ER diagrams.
The objective of this tutorial is to teach the reader the basics of ER diagrams, including symbols, and an understanding of how to translate real world examples into ER diagrams. 
The tutorial will also touch upon techniques for optimising them to make sense logically, and to maximise simplicity. 

This is important because ER diagrams are an important step in moving from abstract concepts to an actual working database. While these can be made less intuitively than making SQL commands, they are not
necessarily optimised or standardised. Thus, a tutorial to both cover the basics and how to use the basics effectively will be useful. 

# How to map ER diagrams to relational models.
The objective of this tutorial is to create a salient bridge between ER diagrams and having a clean, usable database. This will include learning about normal forms,
how to map different types of attributes effectively, and thoughts on how an ER diagram may be modified to make it easier to translate into a good relational diagram. 

 Mapping ER diagrams to relational models is important in reaching an effective database because even if someone understands ER models, they may not realize that they need to transform them 
 into a format that can be effectively sorted through and modified. With this tutorial, readers will understand how to turn an ER model into an effective database. 